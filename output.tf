output "public_ip" {
  value       = aws_instance.terraform_deployed.public_ip
  description = "Public IP Address of EC2 instance"
}

output "instance_id" {
  value       = aws_instance.terraform_deployed.id
  description = "Instance ID"
}
