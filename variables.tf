variable "ami" {
  type        = string
  description = "Amazon-Linux-2"
  default     = "ami-09538990a0c4fe9be"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "Terraform EC2 Instance"
}
