resource "aws_instance" "terraform_deployed" {
  ami           = var.ami //Amazon Linux 2 AMI
  instance_type = var.instance_type
  tags = {
    Name = var.name_tag,
  }
}
